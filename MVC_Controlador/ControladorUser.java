/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MVC_Controlador;

import MVC_Modelo.Usuario;
import MVC_Modelo_Dao.Dao_Usuario;
import MVC_Vista.Crear_Cuenta;
import MVC_Vista.Modulo_Usuario;
import MVC_Vista.Login;
import Metodos_Requeridos.Metodos_vista;
import Patron_Facage.MariaBD;
import javax.swing.JOptionPane;

/**
 *
* @author Yoryi_Gutierrez
 */
public class ControladorUser extends Metodos_vista implements Metodos_Controlador<Usuario> {

    private Usuario user;
    private Modulo_Usuario vista; 
    private MariaBD bd; 
    private Dao_Usuario dao; 

    public Usuario getUser() {
        return user;
    }

    public ControladorUser() {/// Para Crear Usuario Al inicio y para Cambiar pass
        this.bd = new MariaBD("127.0.0.1", "riteve", "root", "");
        this.dao = new Dao_Usuario(this.bd);
    }

    public ControladorUser(Modulo_Usuario vista) {
        this.vista = vista;
        user = new Usuario();
        this.bd = new MariaBD("127.0.0.1", "riteve", "root", "");
        this.dao = new Dao_Usuario(this.bd);
    }

    @Override
    public void insertar(Usuario ob) {
        user = null;
        if (this.dao.ValidarPK(ob)){
            if (dao.insertar(ob)) {
                this.user = ob;
                this.Avisos(12);
            } else {
               this.Avisos(13);
            }
        } else {
             this.Avisos(10);
        }
    }

    @Override
    public void modificar(Usuario ob,String current ) {
        if (this.dao.modificar(ob,current)) {
             this.Avisos(8);
        } else {
             this.Avisos(9);
        }
    }

    @Override
    public void eliminar(Usuario ob) {
        user = null;
        if (this.dao.eliminar(ob)) {
            this.user = ob;
            this.Avisos(11);
        } else {
             this.Avisos(14); 
        }
    }

    public void buscar(Usuario ob) {
      Usuario user = this.dao.buscar(ob);
      Usuario[] Vect = new Usuario[1];
      Vect[0]=user;
        if (user !=null) {
            this.vista.Mostrar(Vect);
        }else{
            this.Avisos(7);
        }
    }

    public Usuario BuscarUserName(Usuario ob) {
        Usuario User = this.dao.BuscarPorUserName(ob);
        return User;
    }

    @Override
    public Usuario Cancelar() {
        if (this.user != null && this.dao.ValidarPK(user)) {
            if (dao.insertar(this.user)) {
                this.Avisos(15);
                this.Mostrar();
                return this.user;
            }
        }
        return null;
    }

    @Override
    public void Filtrar(String st) {
        Usuario[] user = this.dao.Filtrar(st);
       if(user != null){
            this.vista.Mostrar(user);
       }
    }

    public void CambiarPass(Usuario ob, String Name) {
        if (this.dao.CambiarPass(ob, Name)) {
            Object[] mensaje = {"La contraseña se ha cambiado"};
            this.notificar(mensaje);
        } else {
            Object[] mensaje = {"No se ha podido Cambiar la contraseña"};
            this.notificar(mensaje);
        }

    }

    @Override
    public void Mostrar() {
        Usuario[] Valores = this.dao.lista();
        if (Valores != null) {
            this.vista.Mostrar(Valores);
        } else {
            this.Avisos(7);
        }
    }
}
