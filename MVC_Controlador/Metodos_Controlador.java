/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MVC_Controlador;

/**
 *
 * @author Yoryi_Gutierrez
 */
public interface Metodos_Controlador<clase> {

    public void insertar(clase ob);

    public void modificar(clase ob,String current);

    public void eliminar(clase ob);
    
    public void Mostrar() ;
 
    public void Filtrar(String st);

    public clase Cancelar();

}
