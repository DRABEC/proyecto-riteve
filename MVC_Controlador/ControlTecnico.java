/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MVC_Controlador;

import MVC_Modelo.Tecnico;
import MVC_Modelo.Usuario;
import MVC_Modelo_Dao.Dao_Tecnico;
import MVC_Modelo_Dao.Dao_Usuario;
import MVC_Vista.Modulo_Tecnicos;
import MVC_Vista.Modulo_Usuario;
import Metodos_Requeridos.Metodos_vista;
import Patron_Facage.MariaBD;

/**
 *
 ** @author Yoryi_Gutierrez
 */
public class ControlTecnico extends Metodos_vista implements Metodos_Controlador<Tecnico> {
    
    private Tecnico tec; 
    private Modulo_Tecnicos vista; 
    private MariaBD bd;  
    private Dao_Tecnico dao; 

    public Tecnico getTec() {
        return tec;
    }
    
    public ControlTecnico(Modulo_Tecnicos vista) {
        this.vista = vista;
        tec = new Tecnico();
        this.bd = new MariaBD("127.0.0.1", "riteve", "root", "");
        this.dao = new Dao_Tecnico(this.bd);
    }
    
    @Override
    public void insertar(Tecnico ob) {
        if (this.dao.ValidarPK(ob)) {
            if (this.dao.insertar(ob)) {
                this.Avisos(12);
            } else {
               this.Avisos(13);
            }
        } else {
            this.Avisos(10);
        }
        
    }
    
    @Override
    public void modificar(Tecnico ob, String current) {
        if (this.dao.modificar(ob, current)) {
            this.Avisos(8);
        } else {
            this.Avisos(9);
        }
        
    }
    
    @Override
    public void eliminar(Tecnico ob) {
        tec = null;
        if (this.dao.eliminar(ob)) {
            this.tec=ob;
            this.Avisos(11);
        } else {
            this.Avisos(5);
        }
    }
    
    @Override
    public void Mostrar() {
        Object[] valores = this.dao.lista();
        if (valores != null) {
            this.vista.Mostrar(valores);
        } else {
            this.Avisos(7);
        }
    }
    
    @Override
    public void Filtrar(String st) {
        Object[] valores = this.dao.Filtrar(st);
        if (valores != null) {
            this.vista.Mostrar(valores);
        }
    }
    
    @Override
    public Tecnico Cancelar() {
        if (this.tec != null && this.dao.ValidarPK(tec)) {
            if (dao.insertar(this.tec)) {
                this.Avisos(15);
                this.Mostrar();
                return this.tec;
            }
        }
        return null;
    }
    
}
