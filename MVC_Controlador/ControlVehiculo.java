/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MVC_Controlador;

import MVC_Modelo.Tecnico;
import MVC_Modelo.Vehiculo;
import MVC_Modelo_Dao.Dao;
import MVC_Modelo_Dao.Dao_Tecnico;
import MVC_Modelo_Dao.Dao_Vehiculo;
import MVC_Vista.Modulo_Tecnicos;
import MVC_Vista.Modulo_Vehiculos;
import Metodos_Requeridos.Metodos_vista;
import Patron_Facage.MariaBD;

/**
 *
 * * @author Yoryi_Gutierrez
 */
public class ControlVehiculo extends Metodos_vista implements Metodos_Controlador<Vehiculo> {

    private Vehiculo vehiculo;
    private Modulo_Vehiculos vista;
    private MariaBD bd;
    private Dao_Vehiculo dao;

    public ControlVehiculo(Modulo_Vehiculos vista) {
        this.vista = vista;
        vehiculo = new Vehiculo();
        this.bd = new MariaBD("127.0.0.1", "riteve", "root", "");
        this.dao = new Dao_Vehiculo(this.bd);
    }

    @Override
    public void insertar(Vehiculo ob) {
        if(this.dao.ValidarPK(ob)){
            if (this.dao.insertar(ob)) {
            this.Avisos(12);
        } else {
            this.Avisos(13);
        }
        }else{
            this.Avisos(16);
        }
    }

    @Override
    public void modificar(Vehiculo ob, String current) {
        if (this.dao.modificar(ob, current)) {
            this.Avisos(8);
        } else {
            this.Avisos(9);
        }
    }

    @Override
    public void eliminar(Vehiculo ob) {
        vehiculo = null;
        if (this.dao.eliminar(ob)) {
            this.vehiculo = ob;
            this.Avisos(11);
        } else {
            this.Avisos(11);
        }

    }

    @Override
    public void Mostrar() {
        Vehiculo[] valores = this.dao.lista();
        if (valores != null) {
            this.vista.Mostrar(valores);
        } else {
            this.Avisos(7);
        }
    }

    @Override
    public void Filtrar(String st) {
        Object[] valores = this.dao.Filtrar(st);
        if (valores != null) {
            this.vista.Mostrar(valores);
        }

    }

    @Override
    public Vehiculo Cancelar() {
        if (this.vehiculo != null && this.dao.ValidarPK(vehiculo)) {
            if (dao.insertar(this.vehiculo)) {
                this.Avisos(15);
                this.Mostrar();
                return this.vehiculo;
            }
        }
        return null;
    }

}
