/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MVC_Controlador;

import MVC_Modelo.Revisiones;
import MVC_Modelo.Vehiculo;
import MVC_Modelo_Dao.Dao_Revisiones;
import MVC_Modelo_Dao.Dao_Vehiculo;
import MVC_Vista.Modulo_Revisiones;
import MVC_Vista.Modulo_Vehiculos;
import Metodos_Requeridos.Metodos_vista;
import Patron_Facage.MariaBD;

/**
 *
 *  @author Yoryi_Gutierrez
 */
public class ControlRevisiones extends Metodos_vista implements Metodos_Controlador<Revisiones>{
    private Revisiones Rev;
    private Modulo_Revisiones vista;
    private MariaBD bd;
    private Dao_Revisiones  dao;

    public ControlRevisiones(Modulo_Revisiones vista) {
        this.vista = vista;
        Rev = new Revisiones();
        this.bd = new MariaBD("127.0.0.1", "riteve", "root", "");
        this.dao = new Dao_Revisiones(this.bd);
    }

    @Override
    public void insertar(Revisiones ob) {      
        if(this.dao.ValidarPK(ob)){
        if (this.dao.insertar(ob)) {
            this.Avisos(12);
        } else {
            this.Avisos(13);
        }
        }else {
            this.Avisos(16);
        }
    }

    @Override
    public void modificar(Revisiones ob, String current) {
   
       if (this.dao.modificar(ob, current)) {
            this.Avisos(8);
        } else {
            this.Avisos(9);
        }    
    }    

    @Override
    public void eliminar(Revisiones ob) {
   
        Rev = null;
        if (this.dao.eliminar(ob)) {
            this.Rev = ob;
            this.Avisos(11);
        } else {
            this.Avisos(11);
        }
    }

    @Override
    public void Mostrar() {
         Revisiones[] valores = this.dao.lista();
        if (valores != null) {
            this.vista.Mostrar(valores);
        } else {
            this.Avisos(7);
        }
    }

    @Override
    public void Filtrar(String st) {
   
    Object[] valores = this.dao.Filtrar(st);
        if (valores != null) {
            this.vista.Mostrar(valores);
        }
    }

    @Override
    public Revisiones Cancelar() {
   
         if (this.Rev != null && this.dao.ValidarPK(Rev)) {
            if (dao.insertar(this.Rev)) {
                this.Avisos(15);
                this.Mostrar();
                return this.Rev;
            }
        }
        return null;
    }


}
