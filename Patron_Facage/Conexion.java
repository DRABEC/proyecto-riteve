/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Patron_Facage;

import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
* @author Yoryi_Gutierrez
 */
public class Conexion {

    private String driver;
    private String api;
    private String motor;
    private String servidor;
    private String base;
    private String usurio;
    private String password;
    private String Error;

    private Connection cn;
    private PreparedStatement sentencia;
    private ResultSet datos;

    public String getError() {
        return Error;
    }

    public Conexion(String driver, String api, String motor, String servidor, String base, String usurio, String password) {
        this.driver = driver;
        this.api = api;
        this.motor = motor;
        this.servidor = servidor;
        this.base = base;
        this.usurio = usurio;
        this.password = password;
        if (!this.Connect()) {
            throw new NullPointerException();
        }
    }

    public boolean Connect() {
        this.Error = null;
        try {
            Class.forName(this.driver);
            this.cn = DriverManager.getConnection(api + ":" + this.motor + "://" + this.servidor + "/" + this.base, this.usurio, password);
            return true;
        } catch (ClassNotFoundException | SQLException ex) {
            this.Error = ex.toString();
            return false;
        }

    }

    public boolean Preparasentencia(String SQL) {
        this.Error = null;
        try {
            this.sentencia = this.cn.prepareStatement(SQL, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            return true;
        } catch (Exception e) {
            this.Error = e.toString();
            return false;
        }

    }

    public Object[][] seleccionar(Object[] parametros) {
        this.Error = null;
        try {
            this.cargar(parametros);
            this.datos = this.sentencia.executeQuery();
            return this.toArray(this.datos);
        } catch (Exception e) {
            this.Error = e.toString();
            return null;
        }

    }
    
     public boolean ejecutar (Object[] parametros) {
        this.Error = null;
        try {
            this.cargar(parametros);
            return this.sentencia.executeUpdate()>0;
        } catch (Exception e) {
            this.Error = e.toString();
            return false;
        }

    } 
    
    

    private void cargar(Object[] parametros) throws SQLException {
        int i = 1;
        for (Object parametro : parametros) {
            if (parametro instanceof Integer) {
                this.sentencia.setInt(i, (int) parametro);
            }
            if (parametro instanceof Double) {
                this.sentencia.setDouble(i, (Double) parametro);
            }
            if (parametro instanceof String) {
                this.sentencia.setString(i, (String) parametro);
            }
            if (parametro instanceof Date) {
                this.sentencia.setDate(i, (Date) parametro);
            }
            i++;
        }

    }

    private Object[][] toArray(ResultSet rs) throws SQLException {
        rs.last();//ultima linea
        Object[][] datos = new Object[rs.getRow()][rs.getMetaData().getColumnCount()];
        rs.beforeFirst(); //BOF
        int f = 0; //permite mover por las filas
        while (rs.next()) {
            for (int c = 0; c < rs.getMetaData().getColumnCount(); c++) {
                datos[f][c] = rs.getObject(c + 1);
            }
            f++;
        }
        return datos;
    }

}
