/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MVC_Modelo_Dao;

import MVC_Modelo.Tecnico;
import MVC_Modelo.Usuario;
import MVC_Modelo.Vehiculo;
import Patron_Facage.MariaBD;

/**
 *
* @author Yoryi_Gutierrez
 */
public class Dao_Vehiculo implements Dao<Vehiculo> {

    private MariaBD  cn;

    public Dao_Vehiculo(MariaBD bd) {
        this.cn = bd;
    }

    @Override
    public boolean insertar(Vehiculo ob) {
        this.cn.Preparasentencia("INSERT INTO tbvehiculo(NumeroPlaca,Marca,Modelo,Año,FechaInscripcion, Cedula, NombrePropetario) VALUES (?,?,?,?,?,?,?)");
        Object[] param = {ob.getNumeroPlaca(),ob.getMarca(),ob.getModelo(),ob.getAño(),ob.getFechaInscripcion(),ob.getCedula(),ob.getNombrePropetario()};
        return this.cn.ejecutar(param);
    }

    @Override
    public boolean modificar(Vehiculo ob, String current) {
        this.cn.Preparasentencia("UPDATE tbvehiculo SET NumeroPlaca=?,Marca=?,Modelo=?,Año=?,FechaInscripcion=?,Cedula=?,NombrePropetario=? WHERE NumeroPlaca=?");
        Object[] param = {ob.getNumeroPlaca(),ob.getMarca(),ob.getModelo(),ob.getAño(),ob.getFechaInscripcion(),ob.getCedula(),ob.getNombrePropetario(),current};
        Object[][] valores;
        valores = this.cn.seleccionar(param);
        return this.cn.ejecutar(param);
    }

    @Override
    public boolean eliminar(Vehiculo ob) {
        this.cn.Preparasentencia("Delete from  tbvehiculo where NumeroPlaca=?");
        Object[] param = {ob.getNumeroPlaca()};
        return this.cn.ejecutar(param);
    }

    @Override
    public Vehiculo buscar(Vehiculo ob) {
       this.cn.Preparasentencia("Select * from tbvehiculo where NumeroPlaca=? ");
        Object[] param = {ob.getNumeroPlaca()};
        Object[][] valores = this.cn.seleccionar(param);
        if (valores != null && valores.length > 0) { //0,0 valor de cedula
            return new  Vehiculo(String.valueOf(valores[0][1]), String.valueOf(valores[0][2]),
                        String.valueOf(valores[0][3]),(int)valores[0][4],String.valueOf(valores[0][5]),String.valueOf(valores[0][6])
                        ,String.valueOf(valores[0][7]));
        }
        return null;
        
        
        
    }

    @Override
    public Vehiculo[] Filtrar(String Nom) {
       
        this.cn.Preparasentencia("select * from  tbvehiculo  where   NumeroPlaca like (?)  ");
        Object[] param = {Nom};
        Object[][] valores = this.cn.seleccionar(param);
        if (valores != null && valores.length > 0) {
            Vehiculo[] veh = new Vehiculo[valores.length];
            for (int i = 0; i <= valores.length - 1; i++) {
                   veh[i] = new Vehiculo(String.valueOf(valores[i][1]),String.valueOf(valores[i][2])
                        ,String.valueOf(valores[i][3]),(int)valores[i][4],String.valueOf(valores[i][5]),String.valueOf(valores[i][6])
                        ,String.valueOf(valores[i][7]));
            }
            return veh;
        }
        return null;  
        
    }

    @Override
    public Vehiculo[] lista() {
       this.cn.Preparasentencia("select * from  tbvehiculo");
        Object[] param = {};
        Object[][] valores;
        valores = this.cn.seleccionar(param);
        //String NumeroPlaca, String Marca, String Modelo, int Año, String FechaInscripcion, String Cedula, String NombrePropetario
        if (valores != null && valores.length > 0) {
            Vehiculo[] veh = new Vehiculo[valores.length];
            for (int i = 0; i <= valores.length - 1; i++) {
                veh[i] = new Vehiculo(String.valueOf(valores[i][1]),String.valueOf(valores[i][2])
                        ,String.valueOf(valores[i][3]),(int)valores[i][4],String.valueOf(valores[i][5]),String.valueOf(valores[i][6])
                        ,String.valueOf(valores[i][7]));
            }
            return veh;
        }
        return null;

    }

    @Override
    public boolean ValidarPK(Vehiculo ob) {
       return this.buscar(ob)==null;
    }

 

}
