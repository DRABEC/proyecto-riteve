/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MVC_Modelo_Dao;

import MVC_Modelo.Revisiones;
import MVC_Modelo.Tecnico;
import Patron_Facage.MariaBD;
import java.util.Date;

/**
 *
* @author Yoryi_Gutierrez
 */
public class Dao_Revisiones implements Dao<Revisiones> {

    private MariaBD cn;

    public Dao_Revisiones(MariaBD bd) {
        this.cn = bd;
    }

    @Override
    public boolean insertar(Revisiones ob) {
        System.out.println(ob.toString());
        this.cn.Preparasentencia("INSERT INTO tbrevisiones(Hora,Tecnico,Tipo,Observaciones,Estado,Fecha) VALUES (?,?,?,?,?,?)");
        Object[] param = {ob.getHora(), ob.getTecnico(), ob.getTipo(), ob.getObservaciones(), ob.getEstado(),ob.getFecha()};
        return this.cn.ejecutar(param);
    }

    @Override
    public boolean modificar(Revisiones ob, String current) {
    
        this.cn.Preparasentencia("UPDATE tbrevisiones SET Tecnico=?,Hora=?,Tipo=?,Observaciones=?,Estado=?,Fecha=?, WHERE Tecnico=?");
        Object[] param = {ob.getHora(),ob.getTecnico(),ob.getTipo(),ob.getObservaciones(),ob.getEstado(),ob.getFecha(),current};
        Object[][] valores;
        valores = this.cn.seleccionar(param);
        return this.cn.ejecutar(param);
        
    }

    @Override
    public boolean eliminar(Revisiones ob) {
       
        this.cn.Preparasentencia("Delete from  tbrevisiones where Tecnico=?");
        Object[] param = {ob.getTecnico()};
        return this.cn.ejecutar(param);
    }

    @Override
    public Revisiones buscar(Revisiones ob) {
      
        this.cn.Preparasentencia("Select * from tbrevisiones where Tecnico=? ");
        Object[] param = {ob.getTecnico()};
        Object[][] valores = this.cn.seleccionar(param);
        if (valores != null && valores.length > 0) { //0,0 valor de cedula
            return new Revisiones (String.valueOf(valores[0][1]), String.valueOf(valores[0][2]),
                        String.valueOf(valores[0][3]), String.valueOf(valores[0][4]),String.valueOf(valores[0][5]),String.valueOf(valores[0][6]));
                        
        }
        return null;
        
    }

    @Override
    public Revisiones[] Filtrar(String Nom) {
    
        this.cn.Preparasentencia("select * from  tbrevisiones  where Tecnico like (?)  ");
        Object[] param = {Nom};
        Object[][] valores = this.cn.seleccionar(param);
        if (valores != null && valores.length > 0) {
            Revisiones[] rev = new Revisiones[valores.length];
            for (int i = 0; i <= valores.length - 1; i++) {
                   rev[i] = new Revisiones(String.valueOf(valores[i][1]),String.valueOf(valores[i][2])
                        ,String.valueOf(valores[i][3]),String.valueOf(valores[i][4]),String.valueOf(valores[i][5]),String.valueOf(valores[i][6]));
                       
            }
            return rev;
        }
        return null;  

    }

    @Override
    public Revisiones[] lista() {
        this.cn.Preparasentencia("select * from  tbrevisiones");
        Object[] param = {};
        Object[][] valores;
        valores = this.cn.seleccionar(param);
        if (valores != null && valores.length > 0) {
            Revisiones[] Rev = new Revisiones[valores.length];
            for (int i = 0; i <= valores.length - 1; i++) {
                 Rev[i] = new Revisiones(String.valueOf(valores[i][1]),String.valueOf(valores[i][2])
                        ,String.valueOf(valores[i][3]),String.valueOf(valores[i][4]),String.valueOf(valores[i][5]),String.valueOf(valores[i][6]));
                        
            }
            return Rev;
        }
        return null;
    }

    @Override
    public boolean ValidarPK(Revisiones ob) {
    return this.buscar(ob)==null;

    }

}
