/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MVC_Modelo_Dao;

/**
 *
* @author Yoryi_Gutierrez
 */
public interface Dao<clase> {

    public boolean insertar(clase ob);

    public boolean modificar(clase ob, String current);

    public boolean eliminar(clase ob);

    public clase buscar(clase ob);

    public clase[] Filtrar(String Nom);

    public clase[] lista();

    public boolean ValidarPK(clase ob);

//    public boolean ValidarUnicos(clase ob);//campos Unicos
//
//    public boolean ValidarFK(clase ob);
}
