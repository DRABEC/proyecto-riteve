/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MVC_Modelo_Dao;

import MVC_Modelo.Tecnico;
import MVC_Modelo.Usuario;
import Patron_Facage.MariaBD;

/**
 *
* @author Yoryi_Gutierrez
 */
public class Dao_Tecnico implements Dao<Tecnico> {

    private MariaBD cn;

    public Dao_Tecnico(MariaBD bd) {
        this.cn = bd;
    }

    @Override
    public boolean insertar(Tecnico ob) {
        this.cn.Preparasentencia("INSERT INTO tbtecnico(Cedula,Fecha,Telefono,Correo, Salario,Nombre, Apellido) VALUES (?,?,?,?,?,?,?)");
        Object[] param = {ob.getCedula(), ob.getFechaNacimiento(), ob.getTelefono(), ob.getCorreo(), ob.getSalario(), ob.getNombre(), ob.getApellido()};
        System.out.println(ob.toString());
        return this.cn.ejecutar(param);
    }

    @Override
    public boolean modificar(Tecnico ob, String current) {
        this.cn.Preparasentencia("UPDATE tbtecnico SET  Cedula=?,Fecha=?,Telefono=?,Correo=?,Salario=?,Nombre=?,Apellido=? WHERE  Cedula=?");
        Object[] param = {ob.getCedula(), ob.getFechaNacimiento(), ob.getTelefono(), ob.getCorreo(), ob.getSalario(), ob.getNombre(), ob.getApellido(), current};
        Object[][] valores;
        valores = this.cn.seleccionar(param);
        return this.cn.ejecutar(param);
    }

    @Override
    public boolean eliminar(Tecnico ob) {
        this.cn.Preparasentencia("Delete from  tbtecnico where Cedula=?");
        Object[] param = {ob.getCedula()};
        return this.cn.ejecutar(param);
    }

    @Override
    public Tecnico[] Filtrar(String Nom) {
        this.cn.Preparasentencia("select * from  tbtecnico  where  Cedula like (?) order by Cedula ");
        Object[] param = {Nom};
        Object[][] valores = this.cn.seleccionar(param);
        if (valores != null && valores.length > 0) {
            Tecnico[] tec = new Tecnico[valores.length];
            for (int i = 0; i <= valores.length - 1; i++) {
                Double Salario = (Double) valores[i][5];
                tec[i] = new Tecnico(String.valueOf(valores[i][1]), String.valueOf(valores[i][2]),
                        (int) valores[i][3], String.valueOf(valores[i][4]), Salario, String.valueOf(valores[i][6]),
                        String.valueOf(valores[i][7]));
            }

            return tec;
        }
        return null;

    }

    @Override
    public Tecnico[] lista() {
        this.cn.Preparasentencia("select * from  tbtecnico");
        Object[] param = {};
        Object[][] valores;
        valores = this.cn.seleccionar(param);
        if (valores != null && valores.length > 0) {
            Tecnico[] tec = new Tecnico[valores.length];
            for (int i = 0; i <= valores.length - 1; i++) {
                Double Salario = (Double) valores[i][5];
                tec[i] = new Tecnico(String.valueOf(valores[i][1]), String.valueOf(valores[i][2]),
                        (int) valores[i][3], String.valueOf(valores[i][4]), Salario, String.valueOf(valores[i][6]),
                        String.valueOf(valores[i][7]));
            }
            return tec;
        }
        return null;

    }

    @Override
    public boolean ValidarPK(Tecnico ob) {
       return this.buscar(ob)==null;
    }

    @Override
    public Tecnico buscar(Tecnico ob) {
       this.cn.Preparasentencia("select * from tbtecnico where Cedula=? ");
        Object[] param = {ob.getCedula()};
        Object[][] valores = this.cn.seleccionar(param);
        if (valores != null && valores.length > 0) { //0,0 valor de cedula
            Double Salario = (Double) valores[0][5];
            return new Tecnico(String.valueOf(valores[0][1]), String.valueOf(valores[0][2]),
                        (int) valores[0][3], String.valueOf(valores[0][4]), Salario, String.valueOf(valores[0][6]),
                        String.valueOf(valores[0][7]));
        }
        return null;
        
    }

}
