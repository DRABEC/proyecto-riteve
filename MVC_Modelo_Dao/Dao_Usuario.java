/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MVC_Modelo_Dao;

import MVC_Modelo_Dao.Dao;
import MVC_Modelo.Usuario;
import MVC_Modelo.Usuario;
import Patron_Facage.Conexion;

/**
 *
* @author Yoryi_Gutierrez
 */
public class Dao_Usuario implements Dao<Usuario> {

    private Conexion cn;

    public Dao_Usuario(Conexion cn) {
        this.cn = cn;
    }

    @Override
    public boolean insertar(Usuario ob) {
        this.cn.Preparasentencia("insert into user (Nombre,Apellido, Fecha,Telefono,Correo,UserName,Pass,TipoUser,Cedula) VALUES (?,?,?, ?, ?,?, ?, ?, ?)");
        Object[] param = {ob.getNombre(), ob.getApellido(), ob.getFecha(), ob.getTelefono(), ob.getCorreo(), ob.getUserName(), ob.getPass(), ob.getTipoUser(), ob.getCedula()};
        return this.cn.ejecutar(param);
    }

    //User added to Login  
    public Usuario BuscarPorUserName(Usuario ob) {
        this.cn.Preparasentencia("select * from  user where Pass=? and UserName=? ");
        Object[] param = {ob.getPass(), ob.getUserName()};
        Object[][] valores;
        valores = this.cn.seleccionar(param);
        if (valores != null && valores.length > 0) {
            return new Usuario(String.valueOf(valores[0][0]), String.valueOf(valores[0][1]),
                    String.valueOf(valores[0][2]), String.valueOf(valores[0][3]), (int) valores[0][4],
                    String.valueOf(valores[0][5]), String.valueOf(valores[0][6]), String.valueOf(valores[0][7]), String.valueOf(valores[0][8]));
        }
        return null;
    }

    @Override
    public boolean modificar(Usuario ob, String current) {
        this.cn.Preparasentencia("Update user SET Nombre=?,Apellido=?,Fecha=?,Telefono=?,Correo=?,UserName =?, TipoUser=?, Cedula =? WHERE Cedula=? ");
        Object[] param = {ob.getNombre(), ob.getApellido(), ob.getFecha(), ob.getTelefono(), ob.getCorreo(), ob.getUserName(), ob.getTipoUser(), ob.getCedula(), current};
        Object[][] valores;
        valores = this.cn.seleccionar(param);
        return this.cn.ejecutar(param);

    }

    @Override
    public boolean eliminar(Usuario ob) {
        /// No tiene  Requeridos (if)
        this.cn.Preparasentencia("Delete from  user where Cedula=?");
        Object[] param = {ob.getCedula()};
        return this.cn.ejecutar(param);
    }

   
    public Usuario buscar(Usuario ob) {
        this.cn.Preparasentencia("select * from user where Cedula=? ");
        Object[] param = {ob.getCedula()};
        Object[][] valores = this.cn.seleccionar(param);
        if (valores != null && valores.length > 0) { //0,0 valor de cedula
            return new Usuario(String.valueOf(valores[0][9]), String.valueOf(valores[0][1]),
                    String.valueOf(valores[0][2]), String.valueOf(valores[0][3]), (int) valores[0][4],
                    String.valueOf(valores[0][5]), String.valueOf(valores[0][6]), String.valueOf(valores[0][7]), String.valueOf(valores[0][8]));
        }
        return null;

    }

    @Override
    public Usuario[] lista() {
        //order by IdUser
        this.cn.Preparasentencia("select * from  user ");
        Object[] param = {};
        Object[][] valores;
        valores = this.cn.seleccionar(param);
        if (valores != null && valores.length > 0) {
            Usuario[] user = new Usuario[valores.length];
            for (int i = 0; i <= valores.length - 1; i++) {
                user[i] = new Usuario(String.valueOf(valores[i][9]), String.valueOf(valores[i][1]),
                        String.valueOf(valores[i][2]), String.valueOf(valores[i][3]), (int) valores[i][4],
                        String.valueOf(valores[i][5]), String.valueOf(valores[i][6]), String.valueOf(valores[i][7]), String.valueOf(valores[i][8]));
            }
            return user;
        }
        return null;

    }

    public Usuario[] Filtrar(String Nom) {
        this.cn.Preparasentencia("select * from  user  where  Cedula like (?) order by Cedula ");
        Object[] param = {Nom};
        Object[][] valores = this.cn.seleccionar(param);
        if (valores != null && valores.length > 0) {
            Usuario[] user = new Usuario[valores.length];
            for (int i = 0; i <= valores.length - 1; i++) {
                user[i] = new Usuario(String.valueOf(valores[i][9]), String.valueOf(valores[i][1]),
                        String.valueOf(valores[i][2]), String.valueOf(valores[i][3]), (int) valores[i][4],
                        String.valueOf(valores[i][5]), String.valueOf(valores[i][6]), String.valueOf(valores[i][7]), String.valueOf(valores[i][8]));
            }
            return user;
        }
        return null;

    }

    @Override
    public boolean ValidarPK(Usuario ob) {
       return this.buscar(ob) == null;
    }

   

    public Boolean CambiarPass(Usuario ob, String Name) {
        this.cn.Preparasentencia("Update user set Pass=? where UserName=? and Pass=? ");
        Object[] param = {Name, ob.getUserName(), ob.getPass(),};
        Object[][] valores;
        valores = this.cn.seleccionar(param);
        return this.cn.ejecutar(param);
    }

}
