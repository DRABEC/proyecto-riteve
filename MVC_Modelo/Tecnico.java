/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MVC_Modelo;

/**
 *
 * @author Yoryi_Gutierrez
 */
public class Tecnico {
   
     ///Cedula,Fecha,Telefono,Correo, Salario,Nombre, Apellido
    private String Cedula;
    private String FechaNacimiento;
    private int Telefono;
    private String Correo;
    private Double Salario;
    private String Nombre;
    private String Apellido;
       public int getTelefono() {
        return Telefono;
    }

    public void setTelefono(int Telefono) {
        this.Telefono = Telefono;
       
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public String getApellido() {
        return Apellido;
    }

    public void setApellido(String Apellido) {
        this.Apellido = Apellido;
    }

    public String getFechaNacimiento() {
        return FechaNacimiento;
    }

    public void setFechaNacimiento(String FechaNacimiento) {
        this.FechaNacimiento = FechaNacimiento;
    }

    public String getCedula() {
        return Cedula;
    }

    public void setCedula(String Cedula) {
        if(Cedula.length()==9){
           this.Cedula = Cedula;  
        }
    }

    public String getCorreo() {
        return Correo;
    }

    public void setCorreo(String Correo) {
        this.Correo = Correo;
    }

    public Double getSalario() {
        return Salario;
    }

    public void setSalario(Double Salario) {
        this.Salario = Salario;
    }
 
   
       public Tecnico(String Cedula, String FechaNacimiento, int Telefono, String Correo, Double Salario, String Nombre, String Apellido) {
        this.Cedula = Cedula;
        this.FechaNacimiento = FechaNacimiento;
        this.Telefono = Telefono;
        this.Correo = Correo;
        this.Salario = Salario;
        this.Nombre = Nombre;
        this.Apellido = Apellido;
    }
   
    public Tecnico(String cedula) {
       this(cedula, null, 0,null, 0.0, null, null);
    }
      public Tecnico() {
       this(null, null, 0,null, 0.0, null, null);
    }
   

    public boolean requeridos() {
        return false;
       // return this.Nombre != null && Apellido != null && FechaNacimiento != null && Cedula != 0 && Correo != null && Salario != 0.0;
    }

    @Override
    public String toString() {
        return "Tecnico{" + "Nombre=" + Nombre + ", Apellido=" + Apellido + ", FechaNacimiento=" + FechaNacimiento + ", Cedula=" + Cedula + ", Correo=" + Correo + ", Salario=" + Salario + ", Telefono=" + Telefono + '}';
    }

    
    
    
    
    
    
}
