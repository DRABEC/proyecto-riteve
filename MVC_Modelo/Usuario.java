/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MVC_Modelo;

import java.sql.Date;

/**
 *
 * @author Yoryi_Gutierrez
 */
public class Usuario {
    private String Cedula;
    private String Nombre;
    private String Apellido; 
    private String Fecha;
    private int Telefono;
    private String Correo;
    private String UserName;
    private String Pass;
    private String TipoUser;

    public Usuario() {
         this(null,null, null, null, 0, null, null, null, null); 
    }
    
    public String getCedula() {
        return Cedula;
    }

    public void setCedula(String Cedula) {
     //if(Cedula.length()==9)
      this.Cedula = Cedula;
        
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public String getApellido() {
        return Apellido;
    }

    public void setApellido(String Apellido) {
        this.Apellido = Apellido;
    }

    public String getFecha() {
        return Fecha;
    }

    public void setFecha(String Fecha) {
        this.Fecha = Fecha;
    }

    public int getTelefono() {
        return Telefono;
    }

    public void setTelefono(int Telefono) {
        this.Telefono = Telefono;
    }

    public String getCorreo() {
        return Correo;
    }

    public void setCorreo(String Correo) {
        this.Correo = Correo;
    }

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String UserName) {
        this.UserName = UserName;
    }

    public String getPass() {
        return Pass;
    }

    public void setPass(String Pass) {
        this.Pass = Pass;
    }

    public String getTipoUser() {
        return TipoUser;
    }

    public void setTipoUser(String TipoUser) {
        this.TipoUser = TipoUser;
    }

    public Usuario(String Cedula) {
       this(Cedula,null, null, null, 0, null, null, null, null); 
     } 
    public Usuario( String UserName, String Pass) {
       this(null,null, null, null, 0, null, UserName, Pass, null); 
     }
    public Usuario(String Cedula, String Nombre, String Apellido, String Fecha, int Telefono, String Correo, String UserName, String Pass, String TipoUser) {
        this.Cedula = Cedula;
        this.Nombre = Nombre;
        this.Apellido = Apellido;
        this.Fecha = Fecha;
        this.Telefono = Telefono;
        this.Correo = Correo;
        this.UserName = UserName;
        this.Pass = Pass;
        this.TipoUser = TipoUser;
    }
    public Usuario(String Cedula, String Nombre, String Apellido, String Fecha, int Telefono, String Correo, String UserName, String TipoUser) {
        this.Cedula = Cedula;
        this.Nombre = Nombre;
        this.Apellido = Apellido;
        this.Fecha = Fecha;
        this.Telefono = Telefono;
        this.Correo = Correo;
        this.UserName = UserName;
        this.TipoUser = TipoUser;
    }

  
    public boolean requeridos(){
       return this.Cedula!=null && this.Nombre !=null && Apellido !=null && Fecha !=null &&  Telefono !=0 &&
             Correo !=null && UserName !=null && TipoUser  !=null;
    }
}
