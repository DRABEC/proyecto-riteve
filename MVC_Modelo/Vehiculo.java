/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MVC_Modelo;



/**
 *
* @author Yoryi_Gutierrez
 */
public class Vehiculo {

    private String NumeroPlaca;
    private String Marca;
    private String Modelo;
    private int Año;
    private String FechaInscripcion;
    private String Cedula;
    private String NombrePropetario;

    public String getNumeroPlaca() {
        return NumeroPlaca;
    }

    public void setNumeroPlaca(String NumeroPlaca) {
        this.NumeroPlaca = NumeroPlaca;
    }

    public String getMarca() {
        return Marca;
    }

    public void setMarca(String Marca) {
        this.Marca = Marca;
    }

    public String getModelo() {
        return Modelo;
    }

    public void setModelo(String Modelo) {
        this.Modelo = Modelo;
    }

    public int getAño() {
        return Año;
    }

    public void setAño(int Año) {
        this.Año = Año;
    }

    public String getFechaInscripcion() {
        return FechaInscripcion;
    }

    public void setFechaInscripcion(String FechaInscripcion) {
        this.FechaInscripcion = FechaInscripcion;
    }

    public String getCedula() {
        return Cedula;
    }

    public void setCedula(String Cedula) {
        this.Cedula = Cedula;
    }

    public String getNombrePropetario() {
        return NombrePropetario;
    }

    public void setNombrePropetario(String NombrePropetario) {
        this.NombrePropetario = NombrePropetario;
    }

    public Vehiculo(String NumeroPlaca, String Marca, String Modelo, int Año, String FechaInscripcion, String Cedula, String NombrePropetario) {
        this.NumeroPlaca = NumeroPlaca;
        this.Marca = Marca;
        this.Modelo = Modelo;
        this.Año = Año;
        this.FechaInscripcion = FechaInscripcion;
        this.Cedula = Cedula;
        this.NombrePropetario = NombrePropetario;
    }

    public Vehiculo() {
        this(null, null, null, 0, null, null, null);
    }

    public Vehiculo(String Cedula) {
        this(null, null, null, 0, null, Cedula, null);
    }

    public Boolean requeridos() {
        return NumeroPlaca != null && Marca != null && Modelo != null && Año != 0 && FechaInscripcion != null && Cedula != null && NombrePropetario != null;

    }

    @Override
    public String toString() {
        return "Vehiculo{" + "NumeroPlaca=" + NumeroPlaca + ", Marca=" + Marca + ", Modelo=" + Modelo + ", A\u00f1o=" + Año + ", FechaInscripcion=" + FechaInscripcion + ", Cedula=" + Cedula + ", NombrePropetario=" + NombrePropetario + '}';
    }

 
    
    
    
    

}
