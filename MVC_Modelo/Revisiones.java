/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MVC_Modelo;

/**
 *
 * @author Yoryi_Gutierrez
 */
public class Revisiones {
private String Hora;	
private String Tecnico;
private String Tipo;       
private String Observaciones;
private String Estado;
private String Fecha;
private Vehiculo vehiculo;    

    public String getFecha() {
        return Fecha;
    }

    public void setFecha(String Fecha) {
        this.Fecha = Fecha;
    }

    public Revisiones() {
      this(null,null,null, null,null ,null);
    }

    public String getHora() {
        return Hora;
    }

    public void setHora(String Hora) {
        this.Hora = Hora;
    }

    public String getTecnico() {
        return Tecnico;
    }

    public void setTecnico(String Tecnico) {
        this.Tecnico = Tecnico;
    }

    public String getTipo() {
        return Tipo;
    }

    public void setTipo(String Tipo) {
        this.Tipo = Tipo;
    }

    public String getObservaciones() {
        return Observaciones;
    }

    public void setObservaciones(String Observaciones) {
        this.Observaciones = Observaciones;
    }

    public String getEstado() {
        return Estado;
    }

    public void setEstado(String Estado) {
        this.Estado = Estado;
    }

    public Vehiculo getVehiculo() {
        return vehiculo;
    }

    public void setVehiculo(Vehiculo vehiculo) {
        this.vehiculo = vehiculo;
    }

    public Revisiones(String Hora, String Tecnico, String Tipo, String Observaciones, String Estado,String Fecha) {
        this.Hora = Hora;
        this.Tecnico = Tecnico;
        this.Tipo = Tipo;
        this.Observaciones = Observaciones;
        this.Estado = Estado;
        this.Fecha=Fecha;
    }

    @Override
    public String toString() {
        return "Revisiones{" + "Hora=" + Hora + ", Tecnico=" + Tecnico + ", Tipo=" + Tipo + ", Observaciones=" + Observaciones + ", Estado=" + Estado + '}';
    }
  
    public Boolean requeridos() {
        return Hora != null && Tecnico != null && Tipo != null && Observaciones != null && Estado != null && Fecha != null;

    }

    
    
}
