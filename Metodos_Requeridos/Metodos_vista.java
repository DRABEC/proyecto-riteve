/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Metodos_Requeridos;

import javax.swing.JOptionPane;

/**
 *
* @author Yoryi_Gutierrez
 */
public class Metodos_vista {

    public void notificar(Object[] valores) {
        JOptionPane.showMessageDialog(null, valores[0]);
    }

    public int Preguntar(String mensaje) {
        String V[] = {"SI", "NO"};
        int N = JOptionPane.showOptionDialog(null, mensaje, "Titulo", 0, 0, null, V, null);
        return N;
    }

    public void Avisos(int Opc) {
        String mensaje="";
        switch (Opc) {  //Avisos Repetidos
            case 1:
                mensaje = "Verifique las casillas vacias, que los datos esten seleccionados correctamente";
                break;
            case 2:
                mensaje = "Debes Presionar el Botón de Edición Previamente ,si desea Modificar este Registro";
                break;
            case 3:
                mensaje = "Casilla vacia";
                break;
            case 4:
                mensaje = "Por favor Seleccione la Fila que desea Modificar";
                break;
            case 5:
                mensaje = "No se Ha generado Ningún  cambio en el sistema ";
                break;
            case 6:
                mensaje = "Por favor Seleccione la Fila que desea Eliminar";
                break;
            case 7:
                mensaje = "No se han encontrado resultados en la base de datos";
                break;
            case 8:
                mensaje = "El registro se ha Modifcado !Exitosamente";
                break;
            case 9:
                mensaje = "No se ha podido Modificar el registro";
                break;
            case 10:
                mensaje="La cedula ingresada Ya se encuentra en la base de Datos,Reintente Nuevamente Con otra";
                break;
            case 11:
                mensaje="El Registro ha sido Eliminado";
                break;
                
            case 12:
                mensaje="Se ha Agregado un Nuevo Registro";
                break;
           case 13:
                 mensaje="Se ha  podido insertar el  Registro";
                break;
            case 14:
                 mensaje="Se ha  podido  Eliminar Registro";
                break;
            case 15:
                mensaje="Registro se ha vuelto  Agregar";
                break;
            case 16:
                mensaje="El Numero de Placa del Vehiculo Ya se encuentra Registrado en la base de datos ";
                break;
        }
          Object[] mensajes = {mensaje};
          this.notificar(mensajes);         
    }

}
